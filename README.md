# Pet Project cubit_app (Mountain trips app)

Фрейморки: 
  - flutter_bloc
  - bloc
  - equatable
  - http


## Welcome page 
Стартовая страница с кастомным слайдером

<img src="https://gitlab.com/flutter-projects20/preview-projects/-/raw/79954b326d5de843557b851ba5ef40e1a75ae60d/cubit_app/screen1.png" height="500" />


## Home page 
Домашняя страница с загружаемой информацией по API из сети (о трипах в горах - название, стоимость, изображение и т.п.)

<img src="https://gitlab.com/flutter-projects20/preview-projects/-/raw/79954b326d5de843557b851ba5ef40e1a75ae60d/cubit_app/screen2.png" height="500" />


## Detail page 
Детальная страница с описанием загруженного трипа

<img src="https://gitlab.com/flutter-projects20/preview-projects/-/raw/79954b326d5de843557b851ba5ef40e1a75ae60d/cubit_app/screen3.png" height="500" />

## Video


![](https://gitlab.com/flutter-projects20/preview-projects/-/raw/79954b326d5de843557b851ba5ef40e1a75ae60d/cubit_app/cubit_app_preview.mp4)
