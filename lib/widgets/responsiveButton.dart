import 'package:cubit_app/misc/colors.dart';
import 'package:cubit_app/widgets/app_text.dart';
import 'package:flutter/material.dart';

class ResponsiveButton extends StatelessWidget {
  bool isResponsive;
  double? width;

  ResponsiveButton({Key? key, this.width = 120, this.isResponsive = false}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  Container(
        width: isResponsive ? double.maxFinite : width,
        height: 60,
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: AppColors.mainColor),
        child: Row(
          mainAxisAlignment: isResponsive ? MainAxisAlignment.spaceBetween : MainAxisAlignment.center,
          children: [
            if (isResponsive)
              Container(
                  margin: EdgeInsets.only(left: 20),
                  child: AppText(
                    text: 'Book Trip Now',
                    color: Colors.white,
                  )),
            Image.asset('img/button-one.png'),
          ],
        ),

    );
  }
}
