import 'package:cubit_app/widgets/app_text.dart';
import 'package:flutter/material.dart';

import '../misc/colors.dart';

class AppButton extends StatelessWidget {
  String text;
  IconData? icon;
  final Color color;
  final Color backgroundColor;
  double size;
  final Color borderColor;
  bool isIcon;

  AppButton({
    Key? key,
    this.isIcon = false,
    this.text = 'Hola',
    this.icon,
    required this.size,
    required this.color,
    required this.backgroundColor,
    required this.borderColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: size,
      height: size,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        color: backgroundColor,
        border: Border.all(color: borderColor, width: 2),
      ),
      child: Center(
        child: !isIcon ? AppText(text: text, color: color) : Icon(icon, color: color),
      ),
    );
  }
}
